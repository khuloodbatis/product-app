<?php

namespace App\Http\Controllers\API;

use App\Models\User;




use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Console\ClearResetsCommand;
use App\Http\Controllers\API\BaseController as BaseController;


class RegisterController extends BaseController
{
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required |same:password',

        ]);

        if ($validator->fails()) {
            return $this->sendError('please validate error', $validator->errors());
        }

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);                        // I was  changed that to get me token as plainText (accessToken)becuse he use passport
        $success['token'] = $user->createToken('kaolsuusyd')->plainTextToken;
        $success['name'] = $user->name;
        return $this->sendResponse($success, 'user registered successfully');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' =>  $request->password])) {
            $user = Auth::user();                          // I was  changed that to get me token as plainText (accessToken)becuse he use passport
            $success['token'] = $user->createToken('Khausy')->plainTextToken;
            $success['name'] = $user->name;
            return $this->sendResponse($success, 'user login successfully');
        } else {
            return $this->sendResponse('Please check your Auth', ['error' => 'Unauthorised']);
        }
    }
}
